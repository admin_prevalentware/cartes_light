﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using MySql.Data.MySqlClient;
//using System.Data.SqlClient;
//using System.Data;

//namespace LibreriaSQL
//{
//    public class LibMySQL
//    {
//        private MySqlConnection conexion = null;
//        public void conectar(String endpoint, int port, String username, String password, string dbName)
//        {
//            string connectionString = "datasource=" + endpoint + ";port=" + port + ";username=" + username + ";password=" + password + ";database=" + dbName + ";";
//            conexion = new MySqlConnection(connectionString);
//        }

//        public MySqlDataReader query(string query)
//        {
//            MySqlCommand commandDatabase = new MySqlCommand(query, conexion);
//            commandDatabase.CommandTimeout = 60;
//            MySqlDataReader reader;
//            DataTable datos = new DataTable();
//            try
//            {
//                // Abre la base de datos
//                conexion.Open();

//                // Ejecuta la consulta
//                reader = commandDatabase.ExecuteReader();
//                return reader;
//            }
//            catch
//            {
//                return null;
//            }
//        }

//        public DataTable fetchQuery(string fetch_query)
//        {
//            MySqlDataReader reader = query(fetch_query);
//            DataTable datos = new DataTable();
//            try
//            {
//                if (reader.HasRows)
//                {
//                    datos.Load(reader);
//                }
//                else
//                {
//                    Console.WriteLine("No se encontraron datos.");
//                }

//                // Cerrar la conexión
//                conexion.Close();
//                return datos;
//            }
//            catch (Exception ex)
//            {
//                Console.WriteLine(ex.Message);
//                return datos;
//            }
//        }

//        public DataTable fetchAll(string tabla)
//        {
//            string query = "SELECT * FROM " + tabla;
//            return fetchQuery(query);
//        }
//    }
//}
