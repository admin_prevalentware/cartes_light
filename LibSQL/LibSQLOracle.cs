﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace LibreriaSQL
{
    public class LibOracleSQL
    {
        private OracleConnection conexion = null;
        public void conectar(string host, int port, string service, string username, string password)
        {
            string oradb = "Data Source=(DESCRIPTION =" + "(ADDRESS = (PROTOCOL = TCP)(HOST = " + host + ")(PORT = " + port.ToString() + "))" + "(CONNECT_DATA =" + "(SERVER = DEDICATED)" + "(SERVICE_NAME = " + service + ")));" + "User Id= " + username + ";Password=" + password + ";";

            conexion = new OracleConnection(oradb);

        }

        private OracleCommand queryConfig (string query, Dictionary<string, string> parametros)
        {
            OracleCommand commandDatabase = new OracleCommand(query);
            commandDatabase.Connection = conexion;

            foreach (KeyValuePair<string, string> parametro in parametros)
            {
                commandDatabase.Parameters.Add(new OracleParameter(parametro.Key, parametro.Value));
            }
            return commandDatabase;
        }

        public OracleDataReader query(OracleCommand commandDatabase)
        {
           
            conexion.Open();     
            OracleDataReader reader;
            DataTable datos = new DataTable();
            try
            {
                // Ejecuta la consulta
                reader = commandDatabase.ExecuteReader();
                return reader;
            }
            catch
            {
                return null;
            }
        }

        public DataTable fetchQuery(string FileName, Dictionary<string, string> parametros)
        {
            string CurrentPath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            string fetch_query = File.ReadAllText(CurrentPath + "\\SQL\\" + FileName);
            OracleDataReader reader = query(queryConfig(fetch_query, parametros));
            DataTable datos = new DataTable();
            try
            {
                if (reader.HasRows)
                {
                    datos.Load(reader);
                }
                else
                {
                    Console.WriteLine("No se encontraron datos.");
                }

                // Cerrar la conexión
                conexion.Close();
                return datos;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return datos;
            }
        }
    }
}