Una vez clonado el proyecto, se deben instalar los siguientes Nuget para corregir errores.

En RobotCartes:

Clic derecho, manage Nuget package, e instalar:
- Serilog
- Serilog.Sinks.File
- Serilog.Sinks.Console

En GenericLib:

Clic derecho, manage Nuget package, e instalar:
- GenericParsing
- GenericParser
- Serilog
- Serilog.Sinks.File
- Serilog.Sinks.Console
- Microsoft.Office.Interop.Excel