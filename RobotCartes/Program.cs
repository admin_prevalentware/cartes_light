﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cartes
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            SampleGenerico sample = new SampleGenerico();
            sample.Execute();
        }
    }
}
