﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using RPABaseAPI;
using LibreriaSQL;
using GLib;
using System.Xml;
using Serilog;
using System.Data;
using System.Drawing.Imaging;


namespace Cartes
{
    class SampleGenerico : MyCartesProcessBase
    {
        // Inicialización de variables
        // Obligatorias
        private GenericLib fGeneric;
        bool errorShare = false;
        int maxretry;
        int timeout;
        string toReport;

        public SampleGenerico() : base()
        {
            fGeneric = null;
            ShowAbort = true;

        }
        protected override string getNeededRPASuiteVersion()
        {
            return "3.0.1.0";
        }
        protected override string getRPAMainFile()
        {
            return CurrentPath + "\\Cartes\\generic.cartes.rpa";
        }
        protected override void LoadConfiguration(XmlDocument XmlCfg)
        {

        }
        protected override void DoExecute(ref DateTime start)
        {
            try
            {
                generico.Funcion1("https:\\www.google.com", 30);

            }
            finally
            {              
                generico.Close();
            }
        }       
        public GenericLib generico
        {
            get
            {
                if (fGeneric == null)
                    fGeneric = new GenericLib(this);
                return fGeneric;
            }
        }
    }

}
