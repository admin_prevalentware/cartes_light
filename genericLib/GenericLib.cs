﻿using Cartes;
using MiTools;
using RPABaseAPI;
using Serilog;
using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace GLib
{
    public class GenericLib : MyCartesAPIBase
    {
        private static bool loaded = false;

        //Declaración de Variables de Cartes 
        //Ejemplos:
        private RPAWin32Component WinRestaurar = null;

        public GenericLib(MyCartesProcess owner) : base(owner)
        {

        }
        protected override void MergeLibrariesAndLoadVariables()
        {
            //Cargar almenos una variable de Cartes.rpa
            if (!loaded || (Execute("isVariable(\"$WinRestaurar\");") != "1"))
            {
                RPAWin32Component Chrome = null, UrlEdit = null;

                //Carga las variables capturadas con RPA Developer desde la ruta donde se encuentre guardado el archivo .rpa
                //Tener cuidado con la buena ruta, en muchas ocasiones se confunde la carpeta donde realmente se encuentra el archivo

                loaded = cartes.merge(CurrentPath + "\\Cartes\\GenericLib.cartes.rpa") == 1;
            }
            if (WinRestaurar == null)
            {
                //Importar componentes del RPA Developer a variables locales 
                WinRestaurar = (RPAWin32Component)cartes.component("$WinRestaurar");
            }
        }
        protected override string getNeededRPASuiteVersion()
        {
            return "3.0.1.0";
        }

        //Función Ejemplo
        public int Funcion1(string url, int tout)
        {
            void secuenciaFuncion1()
            {
                if (WinRestaurar.ComponentExist(30))
                {
                    WinRestaurar.click(1);
                }
                else
                {
                    MessageBox.Show("Hola Mundo");
                }
            }
            string[] kill = { "F45RUN32", "chrome" };
            restartProcess("Reiniciando chrome", kill, "chrome.exe " + url);
            sequence(secuenciaFuncion1, WinRestaurar, tout, "Iniciando Proceso XYZ", "Proceso XYZ con error");
            return 1;
        }

        #region Funciones Necesarias
        private void sequence(Action seq, RPAWin32Component checkComponent, int tout, string mensajeInicio, string mensajeError)
        {
            bool exit;
            DateTime timeout;
            timeout = DateTime.Now.AddSeconds(tout);
            exit = false;
            do
            {
                CheckAbort();
                cartes.reset(checkComponent.api());
                if (timeout < DateTime.Now) throw new Exception("Time Out");
                else
                {
                    if (checkComponent.componentexist(10) == 1)
                    {
                        cartes.balloon(mensajeInicio);
                        Log.Information(mensajeInicio);
                        seq();
                        exit = true;
                    }
                    else
                    {
                        cartes.balloon(mensajeError);
                        Log.Error(mensajeError);
                        throw new Exception(mensajeError);
                    }
                }
            } while (!exit);
        }

        private void restartProcess(string LogMessage, string[] ProcessNames, string processPath)
        {
            foreach (var Application in ProcessNames)
            {
                try
                {
                    Log.Information(LogMessage);
                    Process[] processInstances = Process.GetProcessesByName(Application);
                    foreach (Process p in processInstances)
                        p.Kill();
                }
                catch
                {
                    //No hace nada
                }
            }
            if (processPath != null)
            {
                cartes.run(processPath);
            }
        }

        public override void Close()
        {
            string[] kill = { "Chrome", "ProcessX", "ProcessY" };
            restartProcess("Cerrando aplicaciones y finalizando proceso aplicaciones", kill, null);
        }
        #endregion
            
        
    }

}

